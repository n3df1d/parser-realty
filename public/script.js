$(document).ready(() => {
    const getJSON = (fileName) => {
        let  request = new XMLHttpRequest();
        request.open('GET', fileName, false);
        request.send(null);
        return JSON.parse(request.responseText);
    }

    let data = getJSON('/export.json');

    let num = 0
    for (let q = 0; q < data[0].length; q++) {
        for (const i of data){
            num++
            $('#table').append($('<tr><td>' + i[q]['title'] + '</td><td class="text-center">' + i[q]['exchange'] + '</td><td class="text-center">' + i[q]['price_num'] + ' ₽</td><td class="text-center">' + i[q]['price_km_sq'] + ' ₽</td><td class="text-center"><a href="' + i[q]['link'] + '" target="_blank" class="btn btn-secondary">Перейти</a></td></tr>'));
        }
    }

    $('#sortable').click((e) => {
        if (e.target.tagName != 'TH') return;
        let th = e.target;
        sortTable(th.cellIndex, th.dataset.type, 'sortable');
    })

    function sortTable(colNum, type, id) {
        let elem = document.getElementById(id)
        let tbody = elem.querySelector('tbody');
        let rowsArray = Array.from(tbody.rows);
        let compare;
        switch (type) {
            case 'number':
                compare = function (rowA, rowB) {
                    return rowA.cells[colNum].innerHTML.replace(/[^0-9]/g, '') - rowB.cells[colNum].innerHTML.replace(/[^0-9]/g, '');
                };
                break;
            case 'string':
                compare = function (rowA, rowB) {
                    return rowA.cells[colNum].innerHTML > rowB.cells[colNum].innerHTML ? 1 : -1;
                };
                break;
        }
        rowsArray.sort(compare);
        tbody.append(...rowsArray);
    }

    $('#update_json').click(() => {
        $.get('update_json', function(data) {
            $('.result').html(data);
            $('#update_json').html('Обновить данные')
            $('#update_json').prop('disabled', false)
        })
        $('#update_json').prop('disabled', true)
        $('#update_json').html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span><span style="margin-left: 10px" class="sr-only">Парсинг данных...</span>')
    })
})

