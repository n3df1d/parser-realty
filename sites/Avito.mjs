import puppeteer from "puppeteer"

const numberWithSpaces = (x) => {return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")}

const Avito = async (num_page = 1) => {
    const browser = await puppeteer.launch()
    let data = []
    const exchange = "Avito"
    const page = await browser.newPage()

    await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36')
    await page.goto(`https://www.avito.ru/moskva/kvartiry/prodam-ASgBAgICAUSSA8YQ?f=ASgBAQICAkSSA8YQwMENuv03AkDKCCSCWYBZjt4OFAI&p=2%2C1&s=0p=${num_page}`)

    const titles = await page.$$eval('.items-items-kAJAg > div > div > div.iva-item-body-KLUuy > div.iva-item-titleStep-pdebR > a > h3', titles => {
        return titles.map(titles => titles.innerText)
    })

    const prices = await page.$$eval('.items-items-kAJAg > div > div > div.iva-item-body-KLUuy > div.iva-item-priceStep-uq2CQ > span', prices => {
        return prices.map(prices => prices.innerText);
    })

    const prices_num = await page.$$eval('.items-items-kAJAg > div > div > div.iva-item-body-KLUuy > div.iva-item-priceStep-uq2CQ > span > span.price-price-JP7qe', prices_num => {
        return prices_num.map(prices_num => prices_num.innerText)
    })

    const prices_km_sq = await page.$$eval('.items-items-kAJAg > div > div > div.iva-item-body-KLUuy > div.iva-item-priceStep-uq2CQ > span > span.price-noaccent-X6dOy.price-normalizedPrice-PplY9.text-text-LurtD.text-size-s-BxGpL', prices_km_sq => {
        return prices_km_sq.map(prices_km_sq => prices_km_sq.innerText)
    })

    const links = await page.$$eval('.items-items-kAJAg > div > div > div.iva-item-body-KLUuy > div.iva-item-titleStep-pdebR > a', links => {
        return links.map(links => links.href);
    })

    // await page.screenshot({path: 'screenshot.png'})

    if (titles.length === prices.length && prices.length === links.length) {
        let count_block = titles.length;
        for (let i = 0; i < count_block; i++) {
            let title = titles[i].replace(/\n/g, ' ')
            // let price = prices[i].replace(/\n/g, '<br>')
            let price_num = numberWithSpaces(prices_num[i].replace(/[^0-9]/g, ''))
            let price_km_sq = numberWithSpaces(prices_km_sq[i].replace(/[^0-9]/g, ''))
            let link = links[i]
            data.push({title, price_num, price_km_sq, exchange, link})
        }
        await browser.close();
    }

    return data
}

// console.log(await Avito())
export default Avito