import puppeteer from "puppeteer"

const isFirstNum = (stroke) => {
  if (stroke.startsWith('1') || stroke.startsWith('2') || stroke.startsWith('3') || stroke.startsWith('4') || stroke.startsWith('5') || stroke.startsWith('6') || stroke.startsWith('7') || stroke.startsWith('8') || stroke.startsWith('9'))
      return true
  else
      return false
}

const numberWithSpaces = (x) => {return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")}

const Cian = async (num_page = 1) => {
    const browser = await puppeteer.launch()
    let data = []
    const exchange = "Циан"
    const page = await browser.newPage()

    await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36')
    await page.goto(`https://www.cian.ru/cat.php?deal_type=sale&engine_version=2&offer_type=flat&only_flat=99&p=${num_page}&region=1&room1=1&room2=1&sort=price_object_order`)

    const titles = await page.$$eval('#frontend-serp > div > div._93444fe79c--wrapper--W0WqH > article > div._93444fe79c--card--ibP42 > div._93444fe79c--content--lXy9G > div._93444fe79c--general--BCXJ4 > div > div:nth-child(2)', titles => {
        return titles.map(titles => titles.innerText)
    })

    const prices = await page.$$eval('#frontend-serp > div > div._93444fe79c--wrapper--W0WqH > article > div._93444fe79c--card--ibP42 > div._93444fe79c--content--lXy9G > div._93444fe79c--general--BCXJ4 > div > div:nth-child(5)', prices => {
        return prices.map(prices => prices.innerText)
    })

    const prices_num = await page.$$eval('#frontend-serp > div > div._93444fe79c--wrapper--W0WqH > article > div._93444fe79c--card--ibP42 > div._93444fe79c--content--lXy9G > div._93444fe79c--general--BCXJ4 > div > div:nth-child(5) > div:nth-child(1)', prices_num => {
        return prices_num.map(prices_num => prices_num.innerText)
    })

    const prices_km_sq = await page.$$eval('#frontend-serp > div > div._93444fe79c--wrapper--W0WqH > article > div._93444fe79c--card--ibP42 > div._93444fe79c--content--lXy9G > div._93444fe79c--general--BCXJ4 > div > div:nth-child(5) > div:nth-child(2)', prices_km_sq => {
        return prices_km_sq.map(prices_km_sq => prices_km_sq.innerText)
    })

    const links = await page.$$eval('#frontend-serp > div > div._93444fe79c--wrapper--W0WqH > article > div._93444fe79c--card--ibP42 > div._93444fe79c--content--lXy9G > div._93444fe79c--general--BCXJ4 > div > a', links => {
        return links.map(links => links.href)
    })

    // await page.screenshot({path: 'screenshot.png'})

    if (titles.length === prices.length && prices.length === links.length) {
        let count_block = titles.length;

        for (let i = 0; i < count_block; i++) {
            if (isFirstNum(prices[i]) && prices[i].length < 90) {
                let title = titles[i].replace(/\n/g, ' ')
                // let price = prices[i].replace(/\n\n/g, '<br>')
                let price_num = numberWithSpaces(prices_num[i].replace(/[^0-9]/g, ''))
                let price_km_sq = numberWithSpaces(prices_km_sq[i].replace(/[^0-9]/g, ''))
                let link = links[i]
                data.push({title, price_num, price_km_sq, exchange, link})
            }
        }
        await browser.close();

    }

    return data

}

export default Cian