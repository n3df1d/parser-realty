import puppeteer from "puppeteer"

const numberWithSpaces = (x) => {return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")}

const Domofond = async (num_page = 1) => {
    const browser = await puppeteer.launch()
    let data = []
    const exchange = "Domofond.ru"
    const page = await browser.newPage()

    await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36')
    await page.goto(`https://www.domofond.ru/prodazha-kvartiry-moskva-c3584?PriceFrom=200000&Rooms=One%2CTwo&ApartmentStyles=Brick%2CModular%2CMonolithic%2CPanel&IsPartOfRenovationProgram=ExcludeParticipating&SortOrder=PriceLow&HasPhotos=True&Page=${num_page}`)

    const titles = await page.$$eval('#root > main > div > div > div.search-results__main___38xtS > div.search-results__itemCardList___RdWje > a > div.long-item-card__information___YXOtb > div.long-item-card__informationHeader___v1HWy > div.long-item-card__informationHeaderRight___3bkKw > span', titles => {
        return titles.map(titles => titles.innerText)
    })

    const prices = await page.$$eval('#root > main > div > div > div.search-results__main___38xtS > div.search-results__itemCardList___RdWje > a > div.long-item-card__information___YXOtb > div.long-item-card__informationHeader___v1HWy > div.long-item-card__informationHeaderLeft___3a-pz', prices => {
        return prices.map(prices => prices.innerText);
    })

    const prices_num = await page.$$eval('#root > main > div > div > div.search-results__main___38xtS > div.search-results__itemCardList___RdWje > a > div.long-item-card__information___YXOtb > div.long-item-card__informationHeader___v1HWy > div.long-item-card__informationHeaderLeft___3a-pz > div.long-item-card__priceContainer___29DcY > span', prices_num => {
        return prices_num.map(prices_num => prices_num.innerText)
    })

    const prices_km_sq = await page.$$eval('#root > main > div > div > div.search-results__main___38xtS > div.search-results__itemCardList___RdWje > a > div.long-item-card__information___YXOtb > div.long-item-card__informationHeader___v1HWy > div.long-item-card__informationHeaderLeft___3a-pz > div.additional-price-info__additionalPriceInfo___lBqNv', prices_km_sq => {
        return prices_km_sq.map(prices_km_sq => prices_km_sq.innerText)
    })

    const links = await page.$$eval('#root > main > div > div > div.search-results__main___38xtS > div.search-results__itemCardList___RdWje > a', links => {
        return links.map(links => links.href);
    })

    // await page.screenshot({path: 'screenshot.png'})

    if (titles.length === prices.length && prices.length === links.length) {
        let count_block = titles.length;
        for (let i = 0; i < count_block; i++) {
            let title = titles[i].replace(/\n/g, ' ')
            // let price = prices[i].replace(/\n/g, '<br>')
            let price_num = numberWithSpaces(prices_num[i].replace(/[^0-9]/g, ''))
            let price_km_sq = numberWithSpaces(prices_km_sq[i].replace(/[^0-9]/g, ''))
            let link = links[i]
            data.push({title, price_num, price_km_sq, exchange, link})
        }
        await browser.close();
    }

    return data
}

// console.log(await Domofond())
export default Domofond