import puppeteer from "puppeteer"

const numberWithSpaces = (x) => {return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")}

const Mirkvartir = async (num_page = 1) => {
    const browser = await puppeteer.launch()
    let data = []
    const exchange = "Мир Квартир"
    const page = await browser.newPage()

    await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36')
    await page.goto(`https://www.mirkvartir.ru/listing/?locationIds=MK_Region%7C77&p=${num_page}&by=2&sort=0&oneRoom=true&twoRooms=true`)

    let titles = await page.$$eval('#l-site-container > div.ContentLayout_layout__P6FRb > div:nth-child(5) > div.LayoutColumnLeft_bColumnLeft__2LHPZ > div > div > div.OffersListItem_infoContainer__1xyCn > div.OffersListItem_content__2LDEz > div.OffersListItem_offerTitleContainer__2netd > a > span', titles => {
        return titles.map(titles => titles.innerText)
    })

    const prices = await page.$$eval('#l-site-container > div.ContentLayout_layout__P6FRb > div:nth-child(5) > div.LayoutColumnLeft_bColumnLeft__2LHPZ > div > div > div.OffersListItem_infoContainer__1xyCn > div.OffersListItem_rightContainer__B0OAY > div.OfferPrice_offerPrice__2kzn3', prices => {
        return prices.map(prices => prices.innerText);
    })

    const prices_num = await page.$$eval('#l-site-container > div.ContentLayout_layout__P6FRb > div:nth-child(5) > div.LayoutColumnLeft_bColumnLeft__2LHPZ > div > div > div.OffersListItem_infoContainer__1xyCn > div.OffersListItem_rightContainer__B0OAY > div.OfferPrice_offerPrice__2kzn3 > span.OfferPrice_price__1jdEj', prices_num => {
        return prices_num.map(prices_num => prices_num.innerText)
    })

    const prices_km_sq = await page.$$eval('#l-site-container > div.ContentLayout_layout__P6FRb > div:nth-child(5) > div.LayoutColumnLeft_bColumnLeft__2LHPZ > div > div > div.OffersListItem_infoContainer__1xyCn > div.OffersListItem_rightContainer__B0OAY > div.OfferPrice_offerPrice__2kzn3 > span.OfferPrice_priceSub__2BKUo > span', prices_km_sq => {
        return prices_km_sq.map(prices_km_sq => prices_km_sq.innerText)
    })

    let links = await page.$$eval('#l-site-container > div.ContentLayout_layout__P6FRb > div:nth-child(5) > div.LayoutColumnLeft_bColumnLeft__2LHPZ > div > div > div.OffersListItem_infoContainer__1xyCn > div.OffersListItem_content__2LDEz > div.OffersListItem_offerTitleContainer__2netd > a', links => {
        return links.map(links => links.href);
    })

    const tel = await page.$$eval('#l-site-container > div.ContentLayout_layout__P6FRb > div:nth-child(5) > div.LayoutColumnLeft_bColumnLeft__2LHPZ > div > div > div.OffersListItem_infoContainer__1xyCn > div.OffersListItem_rightContainer__B0OAY > .OffersListItem_sellerContainer__2EO1Z > .Phone_bSellerPhone__1D82O', tel => {
        return tel.map(tel => tel.innerText)
    })

    // await page.screenshot({path: 'screenshot.png'})

    let i = 0
    for (const t of tel){
        const num = Number(t.replace(/[^0-9]/g, ''))
        if (num != 0){
            titles.splice(i, 1)
            prices.splice(i, 1)
            links.splice(i, 1)
        } else {
            i++
        }
    }

    if (titles.length === prices.length && prices.length === links.length) {
        let count_block = titles.length;
        for (let i = 0; i < count_block; i++) {
            let title = titles[i].replace(/\n/g, ' ')
            // let price = prices[i].replace(/\n/g, '<br>')
            let price_num = numberWithSpaces(prices_num[i].replace(/[^0-9]/g, ''))
            let price_km_sq = numberWithSpaces(prices_km_sq[i].replace(/[^0-9]/g, ''))
            let link = links[i]
            data.push({title, price_num, price_km_sq, exchange, link})
        }
        await browser.close();
    }

    return data
}

export default Mirkvartir

// console.log(await Mirkvartir())