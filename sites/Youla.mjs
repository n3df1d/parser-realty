import puppeteer from "puppeteer"

const numberWithSpaces = (x) => {return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")}

const Youla = async (num_page = 1) => {
    const browser = await puppeteer.launch()
    let data = []
    const exchange = "Юла"
    const page = await browser.newPage()

    await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36')
    await page.goto(`https://youla.ru/moskva/nedvijimost/prodaja-kvartiri?attributes[realty_building_type][0]=166228&attributes[realty_building_type][1]=166229&attributes[sort_field]=price&attributes[price][from]=3000000`)

    const titles = await page.$$eval('#app > div > div.sc-hOzowv.jYNaZW > main > div > div.sc-dEVLtI.dgfFUx > div > section > div.sc-ljHgPW.bkjIug > div.sc-eOZQcF.gHohCr > div > div.sc-eJhAIA.jdQdEZ > div:nth-child(2) > div > div:nth-child(1) > div > span > a > figure > div.sc-gKelgU.joeTcw > figcaption', titles => {
        return titles.map(titles => titles.innerText)
    })

    const prices_num = await page.$$eval('#app > div > div.sc-hOzowv.jYNaZW > main > div > div.sc-dEVLtI.dgfFUx > div > section > div.sc-ljHgPW.bkjIug > div.sc-eOZQcF.gHohCr > div > div.sc-eJhAIA.jdQdEZ > div:nth-child(2) > div > div:nth-child(1) > div > span > a > figure > div.sc-gKelgU.joeTcw > div > div.sc-jRZndg.kkuRyV > p', prices_num => {
        return prices_num.map(prices_num => prices_num.innerText)
    })


    const links = await page.$$eval('#app > div > div.sc-hOzowv.jYNaZW > main > div > div.sc-dEVLtI.dgfFUx > div > section > div.sc-ljHgPW.bkjIug > div.sc-eOZQcF.gHohCr > div > div.sc-eJhAIA.jdQdEZ > div:nth-child(2) > div > div:nth-child(1) > div > span > a', links => {
        return links.map(links => links.href)
    })

    await page.screenshot({path: 'screenshot.png'})

    if (titles.length ===  links.length) {
        let count_block = titles.length;

        for (let i = 0; i < count_block; i++) {
            let title = titles[i].replace(/\n/g, ' ')
                // let price = prices[i].replace(/\n\n/g, '<br>')
            let price_num = numberWithSpaces(prices_num[i].replace(/[^0-9]/g, ''))
            let price_km_sq = "—"
            let link = links[i]
            data.push({title, price_num, price_km_sq, exchange, link})
        }
        await browser.close();

    }

    return data
}

console.log(await Youla())

// export default Youla;