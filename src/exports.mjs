import Cian from "../sites/Cian.mjs"
import Avito from "../sites/Avito.mjs"
import Mirkvartir from "../sites/Mirkvartir.mjs"
import Domofond from "../sites/Domofond.mjs";
import * as fs from 'fs/promises'


const exports = async () => {

    let data_json = []

    for (let i = 1; i <= 20; i++)
    {
        const cian = await Cian(i)
        const avito = await Avito(i)
        const mirkvartir = await Mirkvartir(i)
        const domofond = await Domofond(i)

        data_json.push(cian)
        data_json.push(avito)
        data_json.push(mirkvartir)
        data_json.push(domofond)
    }

    await fs.writeFile('./public/export.json', JSON.stringify(data_json))

    return true
}

export default exports
