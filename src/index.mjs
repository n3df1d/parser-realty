import express from "express"
import { fileURLToPath } from 'url'
import * as path from "path"
import exports from "./exports.mjs";


const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
const app = express();
const urlencoded = express.urlencoded({extended: false})

console.log("Происходит парсинг... Пожалуйста подождите")
await exports()
console.log("Парсинг данных завершён")

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '../public/index.html'))
})

app.get('/script.js',  (req, res) => {
    res.sendFile(path.join(__dirname, '../public/script.js'));
})

app.get('/update_json', async (req, res) => {
    console.log("??? I LOVE CODE <3")
    if (await exports() === true){
        res.send('<pre>Данные успешно спарсились и обновились</pre>')
    } else {
        res.send('<pre>Упс, что-то пошло не так x_x</pre>')
    }
})

app.get('/export.json', (req, res) => {
    res.sendFile(path.join(__dirname, '../public/export.json'));
});

app.listen(3000);
console.log("Сервер готов к работе...");
console.log("Сайт доступен по ссылке http://localhost:3000");